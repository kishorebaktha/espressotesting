package com.example.espressotesting

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        submit.setOnClickListener {
            message.text=firstname.text.toString()+ " "+lastname.text.toString()
            Toast.makeText(this,"reverted changes now", Toast.LENGTH_LONG).show()

        }
    }
}
