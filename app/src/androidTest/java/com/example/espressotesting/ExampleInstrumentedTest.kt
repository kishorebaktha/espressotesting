package com.example.espressotesting


import androidx.test.InstrumentationRegistry
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasEntry
import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Rule
    @JvmField
    val rule:ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java) //start with this activity
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.example.espressotesting", appContext.packageName)
    }

    @Test
    fun user_can_enter_first_name()
    {
        onView(withId(R.id.firstname)).perform(typeText("kishore"))
        //hello
        //hey
    }
    @Test
    fun user_can_enter_last_name()
    {
        onView(withId(R.id.lastname)).perform(typeText("baktha"))
    }
    @Test
    fun check_names()
    {
        onView(withId(R.id.firstname)).perform(typeText("kishore"))
        onView(withId(R.id.lastname)).perform(typeText("baktha"))
        onView(withId(R.id.submit)).perform(click())
        onView(withId(R.id.message)).check(matches(withText("kishore baktha")))
        onView(withText("kishore baktha")).check(matches(isDisplayed()))
    }

}
